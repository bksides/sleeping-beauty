import random
import argparse

def main():
    # Parse the command line arguments...
    parser = argparse.ArgumentParser(description="A Sleeping Beauty problem simulator")
    parser.add_argument("--probability", "-p", default=0.5, type=float, help="The a priori probability that the coin"
                                                                             "will come up tails")
    parser.add_argument("--runs", "-r", default=1, type=int, help="The total number of times to run the experiment")
    parser.add_argument("--verbose", "-v", action="store_true", help="Print the details of each run")
    parser.add_argument("--wakings", "-w", type=int, default=2,
                        help="The number of times to wake Sleeping Beauty if the coin comes up tails")
    parser.add_argument("--strategy", "-s", choices=["interactive", "heads", "tails", "random"], default="interactive",
                        help="The strategy Sleeping Beauty will use when guessing the result of the coin toss")
    parser.add_argument("--per-flip", "-f", action="store_true",
                        help="Measure success by counting the number of flips Sleeping Beauty guesses correctly,"
                             "rather than the number of wakings on which she is correct (the default metric)")
    args = parser.parse_args()

    # interpret the strategy arg
    strategy_map = {"interactive": interactive, "heads": heads, "tails": tails, "random": random_choice}
    strategy = strategy_map[args.strategy]

    total_wins = 0
    total_guesses = 0
    # Do the runs!
    for i in range(args.runs):
        if args.verbose:
            print("\nROUND", i+1)
        result = sleeping_beauty(args.probability, args.wakings, args.verbose, strategy, args.per_flip)
        total_wins += result["wins"]
        total_guesses += result["guesses"]
    print("TOTAL WINS:", total_wins, "/", total_guesses)

# STRATEGIES

# Interactive - the user plays the role of Sleeping Beauty
def interactive():
    print("Pick a result to bet on!  You will be stuck with this choice for all awakenings for this flip, since I can't"
          "wipe your memory and you would otherwise be able to deduce the result of the coin flip after the first"
          "awakening.")
    bet = input("Type 'H' for heads or 'T' for tails: ").lower()[0]
    while bet not in ['h', 't']:
        print("Invalid choice! Try again.")
        bet = input("Type 'H' for heads or 'T' for tails: ").lower()[0]
    return bet == 'h'

def heads():
    return True

def tails():
    return False

def random_choice():
    return bool(random.getrandbits(1))

# Simulates a single round of monty-hall with the given number of doors and the given strategies
def sleeping_beauty(probability, wakings, verbose, strategy, per_flip):
    # first, we flip a coin
    # we get a random float from 0 to 1 and if it's greater than the probability, we get heads
    is_heads = random.random() > probability
    if verbose:
        print("The coin came up", "heads" if is_heads else "tails")
    # ask sleeping beauty for her guess
    wins = 0
    guesses = 1 if (per_flip or is_heads) else wakings
    guess = strategy()
    if verbose:
        print("Sleeping Beauty guessed", "heads" if guess else "tails",
              (str(guesses) + " times") if not per_flip else "")
    if guess == is_heads:
        wins = 1 if (per_flip or is_heads) else wakings
    return {
        "wins": wins,
        "guesses": guesses
    }

if __name__ == "__main__":
    main()