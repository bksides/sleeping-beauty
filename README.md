# Sleeping Beauty Simulator

## Usage

`python3 sleeping_beauty.py [-vf] [-p <probability of heads>] [-r <number of runs>] [-w <number of wakings]
[-s <strategy>]`

## Options

- `--verbose`, `-v`: Print the details of each run
- `--probability`, `-p`: The a priori probability that the coin will land on tails
- `--wakings`, `-w`: The number of wakings that will occur if the coin lands on tails
- `--runs`, `-r`: The total number of times to run the experiment
- `--strategy`, `-s`: The strategy Sleeping Beauty will use when guessing the result of the coin toss.  Possible
  strategies are:
    - `interactive` (default): the user plays the role of the Sleeping Beauty
    - `tails`: Sleeping Beauty will always guess tails
    - `heads`: Sleeping Beauty will always guess heads  
    - `random`: Sleeping Beauty will guess completely randomly
- `--per-flip`, `-f`: Measure success by counting the number of flips Sleeping Beauty guesses correctly,
  rather than the number of wakings on which she is correct (the default metric)
## The Premise

The Sleeping Beauty problem is a controversial problem in probability that, to date, has no generally agreed upon
resolution.  It's interesting for this reason, but also because it has far-reaching implications.  Approaches to
resolving the Sleeping Beauty problem apply to a wide variety of deep questions, right down to the ways in which we
reason about our place in the universe.  Those are quite heady concerns, though; for now, let's just talk about the
actual problem.

We all know the story of Sleeping Beauty: she's cursed to remain in a deep sleep until she's awoken by a handsome
prince.  What many of us don't know, however, is that the witch who cursed her happens to be a dedicated philosopher
of probability in her free time.  As such, she decides to carry out an experiment before putting Sleeping Beauty under
for good.

She puts Sleeping Beauty to sleep once, and then flips a coin.  The coin is fair - it has equal *a priori* probabability
of coming up heads or tails.  Based on the outcome of the coin flip, the witch will do one of two things: if
the coin comes up heads, she wakes Sleeping Beauty only once, and asks her to guess the outcome of the coin toss.  Then
she puts Sleeping Beauty under again, leaving her to await her rescuer.

On the other hand, if the coin comes up tails, she will awaken Sleeping Beauty *twice*.  Each time, she will ask her
to guess the outcome of the coin toss.  The witch's spell has an amnesiac effect, so Sleeping Beauty won't remember
having been woken up.  From her perspective, the two awakenings are identical.  Sleeping Beauty knows the conditions
of the experiment before being put to sleep.

The question is this: given what Sleeping Beauty knows, what should she determine is the probability that the coin
came up heads?  What should she guess to maximize her chances of being right?

## The Problem

There are actually two major answers that have been given to this question, and it's still not generally agreed
upon which is the "right" one.  However, they both seem perfectly obvious.  Maybe you already know which perfectly
obvious answer you'd give.

The first answer is that there is still a 1/2 probability that the coin landed on heads.  After all, the coin was
flipped before Sleeping Beauty was awoken, and the coin was fair.  She knew she would be awoken either way.
So what new information has she gained that could possibly cause her to update her prior probability?

The second answer is that the probability is *1/3*.  After all, there are three possible circumstances in which
Sleeping Beauty would be woken up: two of those would occur after the coin lands on tails, and only one would
occur after it lands on heads.  Since the three scenarios are identical, they should be given equal probability,
meaning that each of them has a 1/3 chance of being the case.  So there is a 1/3 chance that Sleeping Beauty finds
herself in the one scenario where the coin landed on heads.

So who's right?  Well, in a sense, they both are.  These two probabilities give answers to subtly different
questions.  Let's use this simulator to explore that.

## Running the Experiment

Just for now, let's suppose that the second answer is the correct one.  If that's the case, Sleeping Beauty should
always guess "tails", since she'll be right about 2/3 of the time.  Let's run the simulator 100 times and see if
that pans out:

```
python3 sleeping_beauty.py --runs 100 --strategy tails
TOTAL WINS: 94/147
```

94 wins out of 147 guesses works out to roughly 2/3, so it looks like the debate is settled.  But wait - where
did 147 come from?  Didn't we only run the experiment 100 times?  This is true, but remember, in some of those
runs, Sleeping Beauty was woken up *twice*, and hence made more than one guess.  We counted each of those guesses
separately, so that the total ended up being greater than 100.

What happens if we *don't* count those guesses separately?  What if all we're interested in is how many *flips*
Sleeping Beauty guesses correctly, not how many *total times* she guesses correctly?  We can choose to count this
way by specifying the `--per-flip` option:
```
python3 sleeping_beauty.py --runs 100 --strategy tails --per-flip
TOTAL WINS: 51 / 100
```
Now we see that Sleeping Beauty actually only guesses about half the flips correctly, just as we'd expect.  But
in her subjective experience, the coin appears to land on tails about 2/3 of the time.  This is the distinction
between the two interpretations.